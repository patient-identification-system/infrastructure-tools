# Infrastructure Tools

These are the terraform scripts that created the DigitalOcean droplet, along with 
the proper domain name configuration, specs, and DNS.

Dependencies:
- Terraform 1.5+
