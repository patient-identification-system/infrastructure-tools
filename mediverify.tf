# Define the provider and its access token
terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

# Configure the DigitalOcean Provider
provider "digitalocean" {
  token = var.do_token
}

# Create a DigitalOcean droplet
resource "digitalocean_droplet" "medverify_droplet" {
  name              = var.do_droplet_name
  region            = var.do_region
  image             = "debian-11-x64"  # Replace with the latest Debian image ID if needed
  size              = var.do_droplet_size
  ssh_keys          = [39415050]  # An empty list for no SSH keys
}

# Retrieve the IPv4 address of the droplet
data "digitalocean_droplet" "medverify_droplet" {
  id = digitalocean_droplet.medverify_droplet.id
}

# Create a DigitalOcean domain and DNS records
resource "digitalocean_domain" "mediverify_domain" {
  name       = "medverify.online"
  ip_address = data.digitalocean_droplet.medverify_droplet.ipv4_address
}

resource "digitalocean_record" "wildcard_dns" {
  domain = digitalocean_domain.mediverify_domain.name
  type   = "A"
  name   = "*"
  value  = data.digitalocean_droplet.medverify_droplet.ipv4_address
}

resource "digitalocean_project" "MediVerify" {
  name        = "MediVerify"
  description = "CSE 593 Final Project"
  purpose     = "Web Application"
  environment = "Development"
  resources   = [digitalocean_droplet.medverify_droplet.urn, digitalocean_domain.mediverify_domain.urn]
}