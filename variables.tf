variable "do_token" {
    type = string
}

variable "do_region" {
    type = string
}

variable "do_droplet_name" {
    type = string
}

variable "do_droplet_size" {
    type = string
}

variable "domain_name" {
    type = string
}

variable "wildcard_domain_name" {
    type = string
}